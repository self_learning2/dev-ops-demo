package ru.ovechkin.rabbitmq.demo.listener;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import ru.ovechkin.rabbitmq.demo.api.repository.ReviewRepository;
import ru.ovechkin.rabbitmq.demo.consts.Constant;
import ru.ovechkin.rabbitmq.demo.dto.ReviewDTO;
import ru.ovechkin.rabbitmq.demo.entity.Review;
import ru.ovechkin.rabbitmq.demo.exception.IdEmptyException;
import ru.ovechkin.rabbitmq.demo.exception.ReviewIsNullException;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ReviewListener {

    @Setter
    private ReviewRepository reviewRepository;

    @RabbitListener(queues = Constant.DELETE_QUEUE)
    public void deleteById(@Nullable final String reviewId) throws IdEmptyException {
        if (reviewId == null || reviewId.isEmpty()) throw new IdEmptyException();
        log.info("DELETE_QUEUE " + reviewId);
        try {
            reviewRepository.deleteById(reviewId);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
    }

    @RabbitListener(queues = Constant.CREATE_QUEUE)
    public void create(@Nullable final ReviewDTO reviewDTO) throws ReviewIsNullException {
        if (reviewDTO == null) throw new ReviewIsNullException();
        log.info("CREATE_QUEUE " + reviewDTO.toString());
        @NotNull final Review review = reviewDTO.toEntity();
        if (review == null) throw new ReviewIsNullException();
        reviewRepository.save(review);
    }

    @RabbitListener(queues = Constant.UPDATE_QUEUE)
    public List<ReviewDTO> edit(@Nullable final ReviewDTO reviewDTO) throws ReviewIsNullException {
        if (reviewDTO == null) throw new ReviewIsNullException();
        log.info("EDIT_QUEUE " + reviewDTO.toString());
        @NotNull final Review review = reviewDTO.toEntity();
        if (review == null) throw new ReviewIsNullException();
        reviewRepository.save(review);
        return reviewRepository.findAll()
                .stream()
                .map(Review::toDTO)
                .collect(Collectors.toList());
    }

}