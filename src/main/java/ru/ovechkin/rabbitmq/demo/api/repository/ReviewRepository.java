package ru.ovechkin.rabbitmq.demo.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ovechkin.rabbitmq.demo.entity.Review;

@Repository
public interface ReviewRepository extends JpaRepository<Review, String> {
}