FROM openjdk:8-jdk

ADD ./target/dev-ops-demo-0.0.2-SNAPSHOT.war .
ENTRYPOINT exec java -jar ./dev-ops-demo-0.0.2-SNAPSHOT.war